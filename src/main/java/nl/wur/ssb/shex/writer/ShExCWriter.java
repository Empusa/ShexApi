package nl.wur.ssb.shex.writer;

import java.io.Writer;
import java.util.List;
import java.util.Map;

import nl.wur.ssb.RDFSimpleCon.RDFSimpleCon;
import nl.wur.ssb.RDFSimpleCon.ResultLine;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.shex.domain.Annotation;
import nl.wur.ssb.shex.domain.EachOf;
import nl.wur.ssb.shex.domain.NodeConstraint;
import nl.wur.ssb.shex.domain.OneOf;
import nl.wur.ssb.shex.domain.Schema;
import nl.wur.ssb.shex.domain.Shape;
import nl.wur.ssb.shex.domain.ShapeAnd;
import nl.wur.ssb.shex.domain.ShapeOr;
import nl.wur.ssb.shex.domain.TripleConstraint;
import nl.wur.ssb.shex.domain.objectValue;
import nl.wur.ssb.shex.domain.shapeExpr;
import nl.wur.ssb.shex.domain.tripleExpression;
import nl.wur.ssb.shex.domain.valueSetValue;

public class ShExCWriter extends CodeWriter
{
  private Domain domain;
  private Schema root;
  private RDFSimpleCon con;
  private Map<String,String> prefixMap;
  
  public ShExCWriter(Writer out,Domain domain)
  {
    super(out,"  ");
    this.domain = domain;
  }
  
  public void write() throws Exception
  {
    con = domain.getRDFSimpleCon();
    this.prefixMap = con.getPrefixMap();
    for(String prefix : prefixMap.keySet())
    {
      this.writeln("PREFIX " + prefix + ": <" + prefixMap.get(prefix) + ">" );
    }
    this.writeln("");
 
    //this.writeln("start=@" +  con.getShortForm(root.getIRI()));
    int count = 0;
    for(ResultLine line : con.runQuery("getRootSchema.sparql",true))
    {
      if(count++ == 1)
        throw new Exception("there should be only 1 root schema");
      root = domain.make(Schema.class,line.getIRI("schema"));
    }
    if(root == null)
      throw new Exception("No root schema found");
   
    for(shapeExpr shape : root.getAllShapes())
    {
      printShape(shape,true);
      this.newLineIfNot();
      this.writeln("");
    }
  }
  
  public void printShape(shapeExpr shapeExpr,boolean putName) throws Exception
  {
    if(putName)
      this.write(con.getShortForm(shapeExpr.getResource().getURI())+ " ");
    else if(shapeExpr.getIsNamed())
    {
      this.write(con.getShortForm(shapeExpr.getResource().getURI())+ " ");
      return;
    }
    if(shapeExpr instanceof Shape)
    {
      Shape shape = (Shape)shapeExpr;
      this.write("{");
      this.indent("");
      //TODO
      tripleExpression expr = (EachOf)shape.getExpression();
      if(expr != null)
      {
        printTripleExpr(expr,true);
      }
      this.writeln("");
      this.deindent("}");
      printAllAnnotations(shape.getAllAnnotation(),true);
    }
    else if(shapeExpr instanceof ShapeAnd)
    {
      if(!putName)
        this.indent("(");
      ShapeAnd shapeAnd = (ShapeAnd)shapeExpr;
      boolean first = true;
      for(shapeExpr expr : shapeAnd.getAllShapeExprs())
      {
        if(first)
          first = false;
        else
          this.writeln("AND");
        printShape(expr,false);
      }
      if(!putName)
      {
        this.writeln("");
        this.deindent(")");
      }
    }
    else if(shapeExpr instanceof ShapeOr)
    {
      if(!putName)
        this.indent("(");
      ShapeOr shapeOr = (ShapeOr)shapeExpr;
      boolean first = true;
      for(shapeExpr expr : shapeOr.getAllShapeExprs())
      {
        if(first)
          first = false;
        else
          this.writeln("OR");
        printShape(expr,false);
      }
      if(!putName)
      {
        this.writeln("");
        this.deindent(")");
      }
    }
  }
  
  private void printAllAnnotations(List<? extends Annotation> annotations,boolean afterShape) throws Exception
  {
    int count = 1;
    if(annotations.size() > 0 && !afterShape)
      this.writeln("");
    for(Annotation annotation : annotations)
    {
       if(!afterShape)
         this.write("  ");
       this.write("// " + con.getShortForm(annotation.getPredicate()) + " \"" + annotation.getObject().replaceAll("\n","\\\\n")  + "\""); 
       if(count++ < annotations.size() || afterShape)
         this.writeln("");
    }
  }
  
  public void printTripleExpr(tripleExpression expr,boolean isInShape) throws Exception
  {
    if(expr instanceof TripleConstraint)
    {
      TripleConstraint constraint = (TripleConstraint)expr;
      this.write(con.getShortForm(constraint.getPredicate()) + " ");
      shapeExpr oShapeExpr = constraint.getValueExpr();
      if(oShapeExpr instanceof Shape || oShapeExpr instanceof ShapeAnd || oShapeExpr instanceof ShapeOr)
      {
        this.write("@" + con.getShortForm(oShapeExpr.getResource().getURI()));
      }
      else if(oShapeExpr instanceof NodeConstraint)
      {
        NodeConstraint nodeConstraint = (NodeConstraint)oShapeExpr;
        //TODO
        if(nodeConstraint.getDatatype() != null)
          this.write(con.getShortForm(nodeConstraint.getDatatype()));
        else if( nodeConstraint.getAllValues().size() == 0)
        {
          this.write("IRI");
        }
        else
        {
          this.write("[");
          boolean first = true;
          for(valueSetValue value : nodeConstraint.getAllValues())
          {
            //TODO
            if(first)
              first = false;
            else
              this.write(" ");
            objectValue oValue = (objectValue)value;
            this.write(con.getShortForm(oValue.getValue()));
          }
          this.write("]");
        }
      }
      this.write(getMultiplicity(constraint.getMin(),constraint.getMax()));
      printAllAnnotations(constraint.getAllAnnotation(),false);
    }
    else if(expr instanceof EachOf)
    {
      EachOf eachOf = (EachOf)expr;
      if(!isInShape)
        this.indent("(");
      boolean first = true;
      for(tripleExpression subExpr : eachOf.getAllExpressions())
      {
        if(first)
          first = false;
        else
          this.writeln(" ;");
        printTripleExpr(subExpr,true);
      }
      if(!isInShape)
      {
        this.deindent(")" + getMultiplicity(eachOf.getMin(),eachOf.getMax()));
      }
      printAllAnnotations(eachOf.getAllAnnotation(),false);
    }
    else if(expr instanceof OneOf)
    {
      OneOf eachOf = (OneOf)expr;
      if(!isInShape)
        this.indent("(");
      boolean first = true;
      for(tripleExpression subExpr : eachOf.getAllExpressions())
      {
        if(first)
          first = false;
        else
          this.writeln(" |");
        printTripleExpr(subExpr,true);
      }
      if(!isInShape)
      {
        this.deindent(")" + getMultiplicity(eachOf.getMin(),eachOf.getMax()));
      }
      printAllAnnotations(eachOf.getAllAnnotation(),false);
    }
      
  }
  
  public String getMultiplicity(int min,int max)
  {
    if(min == 0 && max == 1)
      return "?";
    else if(min == 0 && max == -1)
      return "*";
    else if(min == 1 && max == -1)
      return "+";
    else if(min == 1 && max == 1)
      return "";
    return "";
  }
  
}
