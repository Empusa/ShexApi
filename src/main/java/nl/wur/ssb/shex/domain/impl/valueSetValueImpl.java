package nl.wur.ssb.shex.domain.impl;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;
import nl.wur.ssb.shex.domain.valueSetValue;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://ssb.wur.nl/shex# ontology
 */
public class valueSetValueImpl extends OWLThingImpl implements valueSetValue {
  public static final String TypeIRI = "http://ssb.wur.nl/shex#valueSetValue";

  protected valueSetValueImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static valueSetValue make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new valueSetValueImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,valueSetValue.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,valueSetValue.class,false);
          if(toRet == null) {
            toRet = new valueSetValueImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof valueSetValue)) {
          throw new RuntimeException("Instance of nl.wur.ssb.shex.domain.impl.valueSetValueImpl expected");
        }
      }
      return (valueSetValue)toRet;
    }
  }

  public void validate() {
  }
}
