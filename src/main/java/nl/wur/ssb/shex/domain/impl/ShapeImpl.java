package nl.wur.ssb.shex.domain.impl;

import java.lang.Boolean;
import java.lang.String;
import java.util.List;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.shex.domain.Annotation;
import nl.wur.ssb.shex.domain.SemAct;
import nl.wur.ssb.shex.domain.Shape;
import nl.wur.ssb.shex.domain.tripleExpression;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://ssb.wur.nl/shex# ontology
 */
public class ShapeImpl extends shapeExprImpl implements Shape {
  public static final String TypeIRI = "http://ssb.wur.nl/shex#Shape";

  protected ShapeImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static Shape make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new ShapeImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,Shape.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,Shape.class,false);
          if(toRet == null) {
            toRet = new ShapeImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof Shape)) {
          throw new RuntimeException("Instance of nl.wur.ssb.shex.domain.impl.ShapeImpl expected");
        }
      }
      return (Shape)toRet;
    }
  }

  public void validate() {
  }

  public tripleExpression getExpression() {
    return this.getRef("http://ssb.wur.nl/shex#expression",true,tripleExpression.class);
  }

  public void setExpression(tripleExpression val) {
    this.setRef("http://ssb.wur.nl/shex#expression",val,tripleExpression.class);
  }

  public void remAnnotation(Annotation val) {
    this.remRef("http://ssb.wur.nl/shex#annotation",val,true);
  }

  public List<? extends Annotation> getAllAnnotation() {
    return this.getRefSet("http://ssb.wur.nl/shex#annotation",true,Annotation.class);
  }

  public void addAnnotation(Annotation val) {
    this.addRef("http://ssb.wur.nl/shex#annotation",val);
  }

  public void remExtra(String val) {
    this.remExternalRef("http://ssb.wur.nl/shex#extra",val,true);
  }

  public List<? extends String> getAllExtra() {
    return this.getExternalRefSet("http://ssb.wur.nl/shex#extra",true);
  }

  public void addExtra(String val) {
    this.addExternalRef("http://ssb.wur.nl/shex#extra",val);
  }

  public void remSemActs(SemAct val) {
    this.remRef("http://ssb.wur.nl/shex#semActs",val,true);
  }

  public List<? extends SemAct> getAllSemActs() {
    return this.getRefSet("http://ssb.wur.nl/shex#semActs",true,SemAct.class);
  }

  public void addSemActs(SemAct val) {
    this.addRef("http://ssb.wur.nl/shex#semActs",val);
  }

  public Boolean getClosed() {
    return this.getBooleanLit("http://ssb.wur.nl/shex#closed",true);
  }

  public void setClosed(Boolean val) {
    this.setBooleanLit("http://ssb.wur.nl/shex#closed",val);
  }

  public Boolean getIsNamed() {
    return this.getBooleanLit("http://ssb.wur.nl/shex#isNamed",false);
  }

  public void setIsNamed(Boolean val) {
    this.setBooleanLit("http://ssb.wur.nl/shex#isNamed",val);
  }
}
