package nl.wur.ssb.shex.domain;

import java.util.List;
import nl.wur.ssb.RDFSimpleCon.api.OWLThing;

/**
 * Code generated from http://ssb.wur.nl/shex# ontology
 */
public interface Schema extends OWLThing {
  void remStartActs(SemAct val);

  List<? extends SemAct> getAllStartActs();

  void addStartActs(SemAct val);

  shapeExpr getStart();

  void setStart(shapeExpr val);

  void remShapes(shapeExpr val);

  List<? extends shapeExpr> getAllShapes();

  void addShapes(shapeExpr val);
}
