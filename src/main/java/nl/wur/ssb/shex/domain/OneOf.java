package nl.wur.ssb.shex.domain;

import java.lang.Integer;
import java.util.List;

/**
 * Code generated from http://ssb.wur.nl/shex# ontology
 */
public interface OneOf extends tripleExpression {
  Integer getMin();

  void setMin(Integer val);

  void remAnnotation(Annotation val);

  List<? extends Annotation> getAllAnnotation();

  void addAnnotation(Annotation val);

  Integer getMax();

  void setMax(Integer val);

  void remExpressions(tripleExpression val);

  List<? extends tripleExpression> getAllExpressions();

  void addExpressions(tripleExpression val);

  void remSemActs(SemAct val);

  List<? extends SemAct> getAllSemActs();

  void addSemActs(SemAct val);
}
