package nl.wur.ssb.shex.domain.impl;

import java.lang.String;
import java.util.List;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;
import nl.wur.ssb.shex.domain.Schema;
import nl.wur.ssb.shex.domain.SemAct;
import nl.wur.ssb.shex.domain.shapeExpr;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://ssb.wur.nl/shex# ontology
 */
public class SchemaImpl extends OWLThingImpl implements Schema {
  public static final String TypeIRI = "http://ssb.wur.nl/shex#Schema";

  protected SchemaImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static Schema make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new SchemaImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,Schema.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,Schema.class,false);
          if(toRet == null) {
            toRet = new SchemaImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof Schema)) {
          throw new RuntimeException("Instance of nl.wur.ssb.shex.domain.impl.SchemaImpl expected");
        }
      }
      return (Schema)toRet;
    }
  }

  public void validate() {
  }

  public void remStartActs(SemAct val) {
    this.remRef("http://ssb.wur.nl/shex#startActs",val,true);
  }

  public List<? extends SemAct> getAllStartActs() {
    return this.getRefSet("http://ssb.wur.nl/shex#startActs",true,SemAct.class);
  }

  public void addStartActs(SemAct val) {
    this.addRef("http://ssb.wur.nl/shex#startActs",val);
  }

  public shapeExpr getStart() {
    return this.getRef("http://ssb.wur.nl/shex#start",true,shapeExpr.class);
  }

  public void setStart(shapeExpr val) {
    this.setRef("http://ssb.wur.nl/shex#start",val,shapeExpr.class);
  }

  public void remShapes(shapeExpr val) {
    this.remRef("http://ssb.wur.nl/shex#shapes",val,true);
  }

  public List<? extends shapeExpr> getAllShapes() {
    return this.getRefSet("http://ssb.wur.nl/shex#shapes",true,shapeExpr.class);
  }

  public void addShapes(shapeExpr val) {
    this.addRef("http://ssb.wur.nl/shex#shapes",val);
  }
}
