package nl.wur.ssb.shex.domain;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.OWLThing;

/**
 * Code generated from http://ssb.wur.nl/shex# ontology
 */
public interface SemAct extends OWLThing {
  String getName();

  void setName(String val);

  String getCode();

  void setCode(String val);
}
