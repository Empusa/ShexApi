package nl.wur.ssb.shex.domain;

import java.lang.String;

/**
 * Code generated from http://ssb.wur.nl/shex# ontology
 */
public interface objectValue extends valueSetValue {
  String getValue();

  void setValue(String val);
}
