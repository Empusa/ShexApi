package nl.wur.ssb.shex.domain;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.OWLThing;

/**
 * Code generated from http://ssb.wur.nl/shex# ontology
 */
public interface Annotation extends OWLThing {
  String getPredicate();

  void setPredicate(String val);

  String getObject();

  void setObject(String val);
}
