package nl.wur.ssb.shex.domain.impl;

import java.lang.Boolean;
import java.lang.String;
import java.util.List;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.shex.domain.NodeConstraint;
import nl.wur.ssb.shex.domain.NodeKind;
import nl.wur.ssb.shex.domain.valueSetValue;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://ssb.wur.nl/shex# ontology
 */
public class NodeConstraintImpl extends shapeExprImpl implements NodeConstraint {
  public static final String TypeIRI = "http://ssb.wur.nl/shex#NodeConstraint";

  protected NodeConstraintImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static NodeConstraint make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new NodeConstraintImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,NodeConstraint.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,NodeConstraint.class,false);
          if(toRet == null) {
            toRet = new NodeConstraintImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof NodeConstraint)) {
          throw new RuntimeException("Instance of nl.wur.ssb.shex.domain.impl.NodeConstraintImpl expected");
        }
      }
      return (NodeConstraint)toRet;
    }
  }

  public void validate() {
  }

  public void remValues(valueSetValue val) {
    this.remRef("http://ssb.wur.nl/shex#values",val,true);
  }

  public List<? extends valueSetValue> getAllValues() {
    return this.getRefSet("http://ssb.wur.nl/shex#values",true,valueSetValue.class);
  }

  public void addValues(valueSetValue val) {
    this.addRef("http://ssb.wur.nl/shex#values",val);
  }

  public NodeKind getNodeKind() {
    return this.getEnum("http://ssb.wur.nl/shex#nodeKind",true,NodeKind.class);
  }

  public void setNodeKind(NodeKind val) {
    this.setEnum("http://ssb.wur.nl/shex#nodeKind",val,NodeKind.class);
  }

  public String getDatatype() {
    return this.getExternalRef("http://ssb.wur.nl/shex#datatype",true);
  }

  public void setDatatype(String val) {
    this.setExternalRef("http://ssb.wur.nl/shex#datatype",val);
  }

  public Boolean getIsNamed() {
    return this.getBooleanLit("http://ssb.wur.nl/shex#isNamed",false);
  }

  public void setIsNamed(Boolean val) {
    this.setBooleanLit("http://ssb.wur.nl/shex#isNamed",val);
  }
}
