package nl.wur.ssb.shex.domain.impl;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;
import nl.wur.ssb.shex.domain.tripleExpression;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://ssb.wur.nl/shex# ontology
 */
public class tripleExpressionImpl extends OWLThingImpl implements tripleExpression {
  public static final String TypeIRI = "http://ssb.wur.nl/shex#tripleExpression";

  protected tripleExpressionImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static tripleExpression make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new tripleExpressionImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,tripleExpression.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,tripleExpression.class,false);
          if(toRet == null) {
            toRet = new tripleExpressionImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof tripleExpression)) {
          throw new RuntimeException("Instance of nl.wur.ssb.shex.domain.impl.tripleExpressionImpl expected");
        }
      }
      return (tripleExpression)toRet;
    }
  }

  public void validate() {
  }
}
