package nl.wur.ssb.shex.domain.impl;

import java.lang.Boolean;
import java.lang.Integer;
import java.lang.String;
import java.util.List;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.shex.domain.Annotation;
import nl.wur.ssb.shex.domain.SemAct;
import nl.wur.ssb.shex.domain.TripleConstraint;
import nl.wur.ssb.shex.domain.shapeExpr;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://ssb.wur.nl/shex# ontology
 */
public class TripleConstraintImpl extends tripleExpressionImpl implements TripleConstraint {
  public static final String TypeIRI = "http://ssb.wur.nl/shex#TripleConstraint";

  protected TripleConstraintImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static TripleConstraint make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new TripleConstraintImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,TripleConstraint.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,TripleConstraint.class,false);
          if(toRet == null) {
            toRet = new TripleConstraintImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof TripleConstraint)) {
          throw new RuntimeException("Instance of nl.wur.ssb.shex.domain.impl.TripleConstraintImpl expected");
        }
      }
      return (TripleConstraint)toRet;
    }
  }

  public void validate() {
    this.checkCardMin1("http://ssb.wur.nl/shex#min");
    this.checkCardMin1("http://ssb.wur.nl/shex#max");
    this.checkCardMin1("http://ssb.wur.nl/shex#predicate");
  }

  public Integer getMin() {
    return this.getIntegerLit("http://ssb.wur.nl/shex#min",false);
  }

  public void setMin(Integer val) {
    this.setIntegerLit("http://ssb.wur.nl/shex#min",val);
  }

  public void remAnnotation(Annotation val) {
    this.remRef("http://ssb.wur.nl/shex#annotation",val,true);
  }

  public List<? extends Annotation> getAllAnnotation() {
    return this.getRefSet("http://ssb.wur.nl/shex#annotation",true,Annotation.class);
  }

  public void addAnnotation(Annotation val) {
    this.addRef("http://ssb.wur.nl/shex#annotation",val);
  }

  public Integer getMax() {
    return this.getIntegerLit("http://ssb.wur.nl/shex#max",false);
  }

  public void setMax(Integer val) {
    this.setIntegerLit("http://ssb.wur.nl/shex#max",val);
  }

  public shapeExpr getValueExpr() {
    return this.getRef("http://ssb.wur.nl/shex#valueExpr",true,shapeExpr.class);
  }

  public void setValueExpr(shapeExpr val) {
    this.setRef("http://ssb.wur.nl/shex#valueExpr",val,shapeExpr.class);
  }

  public void remSemActs(SemAct val) {
    this.remRef("http://ssb.wur.nl/shex#semActs",val,true);
  }

  public List<? extends SemAct> getAllSemActs() {
    return this.getRefSet("http://ssb.wur.nl/shex#semActs",true,SemAct.class);
  }

  public void addSemActs(SemAct val) {
    this.addRef("http://ssb.wur.nl/shex#semActs",val);
  }

  public Boolean getInverse() {
    return this.getBooleanLit("http://ssb.wur.nl/shex#inverse",true);
  }

  public void setInverse(Boolean val) {
    this.setBooleanLit("http://ssb.wur.nl/shex#inverse",val);
  }

  public Boolean getNegated() {
    return this.getBooleanLit("http://ssb.wur.nl/shex#negated",true);
  }

  public void setNegated(Boolean val) {
    this.setBooleanLit("http://ssb.wur.nl/shex#negated",val);
  }

  public String getPredicate() {
    return this.getExternalRef("http://ssb.wur.nl/shex#predicate",false);
  }

  public void setPredicate(String val) {
    this.setExternalRef("http://ssb.wur.nl/shex#predicate",val);
  }
}
