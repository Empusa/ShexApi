package nl.wur.ssb.shex.domain.impl;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.shex.domain.Stem;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://ssb.wur.nl/shex# ontology
 */
public class StemImpl extends valueSetValueImpl implements Stem {
  public static final String TypeIRI = "http://ssb.wur.nl/shex#Stem";

  protected StemImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static Stem make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new StemImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,Stem.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,Stem.class,false);
          if(toRet == null) {
            toRet = new StemImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof Stem)) {
          throw new RuntimeException("Instance of nl.wur.ssb.shex.domain.impl.StemImpl expected");
        }
      }
      return (Stem)toRet;
    }
  }

  public void validate() {
    this.checkCardMin1("http://ssb.wur.nl/shex#stem");
  }

  public String getStem() {
    return this.getStringLit("http://ssb.wur.nl/shex#stem",false);
  }

  public void setStem(String val) {
    this.setStringLit("http://ssb.wur.nl/shex#stem",val);
  }
}
