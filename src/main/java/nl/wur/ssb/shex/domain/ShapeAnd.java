package nl.wur.ssb.shex.domain;

import java.util.List;

/**
 * Code generated from http://ssb.wur.nl/shex# ontology
 */
public interface ShapeAnd extends shapeExpr {
  shapeExpr getShapeExprs(int index);

  List<? extends shapeExpr> getAllShapeExprs();

  void addShapeExprs(shapeExpr val);

  void setShapeExprs(shapeExpr val, int index);

  void remShapeExprs(shapeExpr val);
}
