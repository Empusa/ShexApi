package nl.wur.ssb.shex.domain;

import java.lang.Boolean;
import java.lang.String;
import java.util.List;

/**
 * Code generated from http://ssb.wur.nl/shex# ontology
 */
public interface Shape extends shapeExpr {
  tripleExpression getExpression();

  void setExpression(tripleExpression val);

  void remAnnotation(Annotation val);

  List<? extends Annotation> getAllAnnotation();

  void addAnnotation(Annotation val);

  void remExtra(String val);

  List<? extends String> getAllExtra();

  void addExtra(String val);

  void remSemActs(SemAct val);

  List<? extends SemAct> getAllSemActs();

  void addSemActs(SemAct val);

  Boolean getClosed();

  void setClosed(Boolean val);
}
