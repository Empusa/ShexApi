package nl.wur.ssb.shex.domain;

import java.lang.Boolean;
import java.lang.Integer;
import java.lang.String;
import java.util.List;

/**
 * Code generated from http://ssb.wur.nl/shex# ontology
 */
public interface TripleConstraint extends tripleExpression {
  Integer getMin();

  void setMin(Integer val);

  void remAnnotation(Annotation val);

  List<? extends Annotation> getAllAnnotation();

  void addAnnotation(Annotation val);

  Integer getMax();

  void setMax(Integer val);

  shapeExpr getValueExpr();

  void setValueExpr(shapeExpr val);

  void remSemActs(SemAct val);

  List<? extends SemAct> getAllSemActs();

  void addSemActs(SemAct val);

  Boolean getInverse();

  void setInverse(Boolean val);

  Boolean getNegated();

  void setNegated(Boolean val);

  String getPredicate();

  void setPredicate(String val);
}
