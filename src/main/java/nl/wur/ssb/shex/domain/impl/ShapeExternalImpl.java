package nl.wur.ssb.shex.domain.impl;

import java.lang.Boolean;
import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.shex.domain.ShapeExternal;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://ssb.wur.nl/shex# ontology
 */
public class ShapeExternalImpl extends shapeExprImpl implements ShapeExternal {
  public static final String TypeIRI = "http://ssb.wur.nl/shex#ShapeExternal";

  protected ShapeExternalImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static ShapeExternal make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new ShapeExternalImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,ShapeExternal.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,ShapeExternal.class,false);
          if(toRet == null) {
            toRet = new ShapeExternalImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof ShapeExternal)) {
          throw new RuntimeException("Instance of nl.wur.ssb.shex.domain.impl.ShapeExternalImpl expected");
        }
      }
      return (ShapeExternal)toRet;
    }
  }

  public void validate() {
  }

  public Boolean getIsNamed() {
    return this.getBooleanLit("http://ssb.wur.nl/shex#isNamed",false);
  }

  public void setIsNamed(Boolean val) {
    this.setBooleanLit("http://ssb.wur.nl/shex#isNamed",val);
  }
}
