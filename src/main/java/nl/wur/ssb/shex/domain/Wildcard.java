package nl.wur.ssb.shex.domain;

import nl.wur.ssb.RDFSimpleCon.api.OWLThing;

/**
 * Code generated from http://ssb.wur.nl/shex# ontology
 */
public interface Wildcard extends OWLThing {
}
