package nl.wur.ssb.shex.domain.impl;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;
import nl.wur.ssb.shex.domain.Wildcard;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://ssb.wur.nl/shex# ontology
 */
public class WildcardImpl extends OWLThingImpl implements Wildcard {
  public static final String TypeIRI = "http://ssb.wur.nl/shex#Wildcard";

  protected WildcardImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static Wildcard make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new WildcardImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,Wildcard.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,Wildcard.class,false);
          if(toRet == null) {
            toRet = new WildcardImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof Wildcard)) {
          throw new RuntimeException("Instance of nl.wur.ssb.shex.domain.impl.WildcardImpl expected");
        }
      }
      return (Wildcard)toRet;
    }
  }

  public void validate() {
  }
}
