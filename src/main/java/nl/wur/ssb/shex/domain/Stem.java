package nl.wur.ssb.shex.domain;

import java.lang.String;

/**
 * Code generated from http://ssb.wur.nl/shex# ontology
 */
public interface Stem extends valueSetValue {
  String getStem();

  void setStem(String val);
}
