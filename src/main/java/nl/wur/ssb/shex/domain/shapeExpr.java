package nl.wur.ssb.shex.domain;

import java.lang.Boolean;
import nl.wur.ssb.RDFSimpleCon.api.OWLThing;

/**
 * Code generated from http://ssb.wur.nl/shex# ontology
 */
public interface shapeExpr extends OWLThing {
  Boolean getIsNamed();

  void setIsNamed(Boolean val);
}
