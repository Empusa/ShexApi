package nl.wur.ssb.shex.domain.impl;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;
import nl.wur.ssb.shex.domain.SemAct;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://ssb.wur.nl/shex# ontology
 */
public class SemActImpl extends OWLThingImpl implements SemAct {
  public static final String TypeIRI = "http://ssb.wur.nl/shex#SemAct";

  protected SemActImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static SemAct make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new SemActImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,SemAct.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,SemAct.class,false);
          if(toRet == null) {
            toRet = new SemActImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof SemAct)) {
          throw new RuntimeException("Instance of nl.wur.ssb.shex.domain.impl.SemActImpl expected");
        }
      }
      return (SemAct)toRet;
    }
  }

  public void validate() {
    this.checkCardMin1("http://ssb.wur.nl/shex#name");
  }

  public String getName() {
    return this.getExternalRef("http://ssb.wur.nl/shex#name",false);
  }

  public void setName(String val) {
    this.setExternalRef("http://ssb.wur.nl/shex#name",val);
  }

  public String getCode() {
    return this.getStringLit("http://ssb.wur.nl/shex#code",true);
  }

  public void setCode(String val) {
    this.setStringLit("http://ssb.wur.nl/shex#code",val);
  }
}
