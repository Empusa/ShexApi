package nl.wur.ssb.shex.domain;

import java.lang.String;
import java.util.List;

/**
 * Code generated from http://ssb.wur.nl/shex# ontology
 */
public interface NodeConstraint extends shapeExpr {
  void remValues(valueSetValue val);

  List<? extends valueSetValue> getAllValues();

  void addValues(valueSetValue val);

  NodeKind getNodeKind();

  void setNodeKind(NodeKind val);

  String getDatatype();

  void setDatatype(String val);
}
