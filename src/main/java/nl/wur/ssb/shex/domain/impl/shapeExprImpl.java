package nl.wur.ssb.shex.domain.impl;

import java.lang.Boolean;
import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;
import nl.wur.ssb.shex.domain.shapeExpr;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://ssb.wur.nl/shex# ontology
 */
public class shapeExprImpl extends OWLThingImpl implements shapeExpr {
  public static final String TypeIRI = "http://ssb.wur.nl/shex#shapeExpr";

  protected shapeExprImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static shapeExpr make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new shapeExprImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,shapeExpr.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,shapeExpr.class,false);
          if(toRet == null) {
            toRet = new shapeExprImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof shapeExpr)) {
          throw new RuntimeException("Instance of nl.wur.ssb.shex.domain.impl.shapeExprImpl expected");
        }
      }
      return (shapeExpr)toRet;
    }
  }

  public void validate() {
    this.checkCardMin1("http://ssb.wur.nl/shex#isNamed");
  }

  public Boolean getIsNamed() {
    return this.getBooleanLit("http://ssb.wur.nl/shex#isNamed",false);
  }

  public void setIsNamed(Boolean val) {
    this.setBooleanLit("http://ssb.wur.nl/shex#isNamed",val);
  }
}
