package nl.wur.ssb.shex.domain.impl;

import java.lang.Boolean;
import java.lang.String;
import java.util.List;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.shex.domain.ShapeAnd;
import nl.wur.ssb.shex.domain.shapeExpr;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://ssb.wur.nl/shex# ontology
 */
public class ShapeAndImpl extends shapeExprImpl implements ShapeAnd {
  public static final String TypeIRI = "http://ssb.wur.nl/shex#ShapeAnd";

  protected ShapeAndImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static ShapeAnd make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new ShapeAndImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,ShapeAnd.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,ShapeAnd.class,false);
          if(toRet == null) {
            toRet = new ShapeAndImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof ShapeAnd)) {
          throw new RuntimeException("Instance of nl.wur.ssb.shex.domain.impl.ShapeAndImpl expected");
        }
      }
      return (ShapeAnd)toRet;
    }
  }

  public void validate() {
    this.checkCardMin1("http://ssb.wur.nl/shex#shapeExprs");
  }

  public shapeExpr getShapeExprs(int index) {
    return this.getRefListAtIndex("http://ssb.wur.nl/shex#shapeExprs",false,shapeExpr.class,index);
  }

  public List<? extends shapeExpr> getAllShapeExprs() {
    return this.getRefList("http://ssb.wur.nl/shex#shapeExprs",false,shapeExpr.class);
  }

  public void addShapeExprs(shapeExpr val) {
    this.addRefList("http://ssb.wur.nl/shex#shapeExprs",val);
  }

  public void setShapeExprs(shapeExpr val, int index) {
    this.setRefList("http://ssb.wur.nl/shex#shapeExprs",val,false,index);
  }

  public void remShapeExprs(shapeExpr val) {
    this.remRefList("http://ssb.wur.nl/shex#shapeExprs",val,false);
  }

  public Boolean getIsNamed() {
    return this.getBooleanLit("http://ssb.wur.nl/shex#isNamed",false);
  }

  public void setIsNamed(Boolean val) {
    this.setBooleanLit("http://ssb.wur.nl/shex#isNamed",val);
  }
}
