package nl.wur.ssb.shex.domain.impl;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.shex.domain.objectValue;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://ssb.wur.nl/shex# ontology
 */
public class objectValueImpl extends valueSetValueImpl implements objectValue {
  public static final String TypeIRI = "http://ssb.wur.nl/shex#objectValue";

  protected objectValueImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static objectValue make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new objectValueImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,objectValue.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,objectValue.class,false);
          if(toRet == null) {
            toRet = new objectValueImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof objectValue)) {
          throw new RuntimeException("Instance of nl.wur.ssb.shex.domain.impl.objectValueImpl expected");
        }
      }
      return (objectValue)toRet;
    }
  }

  public void validate() {
    this.checkCardMin1("http://ssb.wur.nl/shex#value");
  }

  public String getValue() {
    return this.getStringLit("http://ssb.wur.nl/shex#value",false);
  }

  public void setValue(String val) {
    this.setStringLit("http://ssb.wur.nl/shex#value",val);
  }
}
