package nl.wur.ssb.shex.domain.impl;

import java.lang.Integer;
import java.lang.String;
import java.util.List;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.shex.domain.Annotation;
import nl.wur.ssb.shex.domain.OneOf;
import nl.wur.ssb.shex.domain.SemAct;
import nl.wur.ssb.shex.domain.tripleExpression;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://ssb.wur.nl/shex# ontology
 */
public class OneOfImpl extends tripleExpressionImpl implements OneOf {
  public static final String TypeIRI = "http://ssb.wur.nl/shex#OneOf";

  protected OneOfImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static OneOf make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new OneOfImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,OneOf.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,OneOf.class,false);
          if(toRet == null) {
            toRet = new OneOfImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof OneOf)) {
          throw new RuntimeException("Instance of nl.wur.ssb.shex.domain.impl.OneOfImpl expected");
        }
      }
      return (OneOf)toRet;
    }
  }

  public void validate() {
    this.checkCardMin1("http://ssb.wur.nl/shex#min");
    this.checkCardMin1("http://ssb.wur.nl/shex#max");
    this.checkCardMin1("http://ssb.wur.nl/shex#expressions");
  }

  public Integer getMin() {
    return this.getIntegerLit("http://ssb.wur.nl/shex#min",false);
  }

  public void setMin(Integer val) {
    this.setIntegerLit("http://ssb.wur.nl/shex#min",val);
  }

  public void remAnnotation(Annotation val) {
    this.remRef("http://ssb.wur.nl/shex#annotation",val,true);
  }

  public List<? extends Annotation> getAllAnnotation() {
    return this.getRefSet("http://ssb.wur.nl/shex#annotation",true,Annotation.class);
  }

  public void addAnnotation(Annotation val) {
    this.addRef("http://ssb.wur.nl/shex#annotation",val);
  }

  public Integer getMax() {
    return this.getIntegerLit("http://ssb.wur.nl/shex#max",false);
  }

  public void setMax(Integer val) {
    this.setIntegerLit("http://ssb.wur.nl/shex#max",val);
  }

  public void remExpressions(tripleExpression val) {
    this.remRef("http://ssb.wur.nl/shex#expressions",val,false);
  }

  public List<? extends tripleExpression> getAllExpressions() {
    return this.getRefSet("http://ssb.wur.nl/shex#expressions",false,tripleExpression.class);
  }

  public void addExpressions(tripleExpression val) {
    this.addRef("http://ssb.wur.nl/shex#expressions",val);
  }

  public void remSemActs(SemAct val) {
    this.remRef("http://ssb.wur.nl/shex#semActs",val,true);
  }

  public List<? extends SemAct> getAllSemActs() {
    return this.getRefSet("http://ssb.wur.nl/shex#semActs",true,SemAct.class);
  }

  public void addSemActs(SemAct val) {
    this.addRef("http://ssb.wur.nl/shex#semActs",val);
  }
}
