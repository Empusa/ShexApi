package nl.wur.ssb.shex.domain.impl;

import java.lang.Boolean;
import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.shex.domain.ShapeNot;
import nl.wur.ssb.shex.domain.shapeExpr;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://ssb.wur.nl/shex# ontology
 */
public class ShapeNotImpl extends shapeExprImpl implements ShapeNot {
  public static final String TypeIRI = "http://ssb.wur.nl/shex#ShapeNot";

  protected ShapeNotImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static ShapeNot make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new ShapeNotImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,ShapeNot.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,ShapeNot.class,false);
          if(toRet == null) {
            toRet = new ShapeNotImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof ShapeNot)) {
          throw new RuntimeException("Instance of nl.wur.ssb.shex.domain.impl.ShapeNotImpl expected");
        }
      }
      return (ShapeNot)toRet;
    }
  }

  public void validate() {
    this.checkCardMin1("http://ssb.wur.nl/shex#shapeExpr");
  }

  public shapeExpr getShapeExpr() {
    return this.getRef("http://ssb.wur.nl/shex#shapeExpr",false,shapeExpr.class);
  }

  public void setShapeExpr(shapeExpr val) {
    this.setRef("http://ssb.wur.nl/shex#shapeExpr",val,shapeExpr.class);
  }

  public Boolean getIsNamed() {
    return this.getBooleanLit("http://ssb.wur.nl/shex#isNamed",false);
  }

  public void setIsNamed(Boolean val) {
    this.setBooleanLit("http://ssb.wur.nl/shex#isNamed",val);
  }
}
