package nl.wur.ssb.shex.domain;

/**
 * Code generated from http://ssb.wur.nl/shex# ontology
 */
public interface ShapeNot extends shapeExpr {
  shapeExpr getShapeExpr();

  void setShapeExpr(shapeExpr val);
}
