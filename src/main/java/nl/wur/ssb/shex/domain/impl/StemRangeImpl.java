package nl.wur.ssb.shex.domain.impl;

import java.lang.String;
import java.util.List;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.shex.domain.StemRange;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://ssb.wur.nl/shex# ontology
 */
public class StemRangeImpl extends valueSetValueImpl implements StemRange {
  public static final String TypeIRI = "http://ssb.wur.nl/shex#StemRange";

  protected StemRangeImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static StemRange make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new StemRangeImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,StemRange.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,StemRange.class,false);
          if(toRet == null) {
            toRet = new StemRangeImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof StemRange)) {
          throw new RuntimeException("Instance of nl.wur.ssb.shex.domain.impl.StemRangeImpl expected");
        }
      }
      return (StemRange)toRet;
    }
  }

  public void validate() {
    this.checkCardMin1("http://ssb.wur.nl/shex#stem");
  }

  public String getStem() {
    return this.getStringLit("http://ssb.wur.nl/shex#stem",false);
  }

  public void setStem(String val) {
    this.setStringLit("http://ssb.wur.nl/shex#stem",val);
  }

  public void remExclusion(String val) {
    this.remStringLit("http://ssb.wur.nl/shex#exclusion",val,true);
  }

  public List<? extends String> getAllExclusion() {
    return this.getStringLitSet("http://ssb.wur.nl/shex#exclusion",true);
  }

  public void addExclusion(String val) {
    this.addStringLit("http://ssb.wur.nl/shex#exclusion",val);
  }
}
