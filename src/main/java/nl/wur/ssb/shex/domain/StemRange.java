package nl.wur.ssb.shex.domain;

import java.lang.String;
import java.util.List;

/**
 * Code generated from http://ssb.wur.nl/shex# ontology
 */
public interface StemRange extends valueSetValue {
  String getStem();

  void setStem(String val);

  void remExclusion(String val);

  List<? extends String> getAllExclusion();

  void addExclusion(String val);
}
