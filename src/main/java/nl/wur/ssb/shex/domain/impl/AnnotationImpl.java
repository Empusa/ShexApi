package nl.wur.ssb.shex.domain.impl;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;
import nl.wur.ssb.shex.domain.Annotation;
import org.apache.jena.rdf.model.Resource;

/**
 * Code generated from http://ssb.wur.nl/shex# ontology
 */
public class AnnotationImpl extends OWLThingImpl implements Annotation {
  public static final String TypeIRI = "http://ssb.wur.nl/shex#Annotation";

  protected AnnotationImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static Annotation make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new AnnotationImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,Annotation.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,Annotation.class,false);
          if(toRet == null) {
            toRet = new AnnotationImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof Annotation)) {
          throw new RuntimeException("Instance of nl.wur.ssb.shex.domain.impl.AnnotationImpl expected");
        }
      }
      return (Annotation)toRet;
    }
  }

  public void validate() {
    this.checkCardMin1("http://ssb.wur.nl/shex#predicate");
    this.checkCardMin1("http://ssb.wur.nl/shex#object");
  }

  public String getPredicate() {
    return this.getExternalRef("http://ssb.wur.nl/shex#predicate",false);
  }

  public void setPredicate(String val) {
    this.setExternalRef("http://ssb.wur.nl/shex#predicate",val);
  }

  public String getObject() {
    return this.getStringLit("http://ssb.wur.nl/shex#object",false);
  }

  public void setObject(String val) {
    this.setStringLit("http://ssb.wur.nl/shex#object",val);
  }
}
