package nl.wur.ssb.shex.domain;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.EnumClass;

/**
 * Code generated from http://ssb.wur.nl/shex# ontology
 */
public enum NodeKind implements EnumClass {
  NonLiteral("http://ssb.wur.nl/shex#NonLiteral",new NodeKind[]{}),

  BNode("http://ssb.wur.nl/shex#BNode",new NodeKind[]{}),

  Literal("http://ssb.wur.nl/shex#Literal",new NodeKind[]{}),

  IRI("http://ssb.wur.nl/shex#IRI",new NodeKind[]{});

  private NodeKind[] parents;

  private String iri;

  private NodeKind(String iri, NodeKind[] parents) {
    this.iri = iri;
    this.parents = parents;
  }

  public EnumClass[] getParents() {
    return parents;
  }

  public String getIRI() {
    return this.iri;
  }

  public static NodeKind make(String iri) {
    for(NodeKind item : NodeKind.values()) {
      if(item.iri.equals(iri)) {
        return item;
      }
    }
    throw new RuntimeException("Enum value: " + iri + " not found for enum type: EnumExample");
  }
}
