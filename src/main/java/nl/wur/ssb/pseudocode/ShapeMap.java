package nl.wur.ssb.pseudocode;

import java.util.HashMap;

import nl.wur.ssb.shex.domain.shapeExpr;

public class ShapeMap
{
  HashMap<String,ShapeMapItem> shapeMaps = new HashMap<String,ShapeMapItem>();
  
  public ShapeMapItem getOpenShapeMapItem()
  {
    //PSEUDO: Get shape map item with the state open, return null if none
    //item is in open 
    for(ShapeMapItem item : shapeMaps.values())
    {
      if(item.state.opType == "open")
        return item;
    }
    return null;
  }
  
  public ShapeMapItem getMakeMapItem(shapeExpr/*exclude node constraint*/ shape,String node)
  {
    //PSEUDO: Get the shape map item if shape to node mapping exists otherwise create a new one
    return null;
  }
}
