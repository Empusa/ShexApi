package nl.wur.ssb.pseudocode;

import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.StmtIterator;

import nl.wur.ssb.RDFSimpleCon.RDFSimpleCon;
import nl.wur.ssb.shex.domain.EachOf;
import nl.wur.ssb.shex.domain.NodeConstraint;
import nl.wur.ssb.shex.domain.OneOf;
import nl.wur.ssb.shex.domain.Shape;
import nl.wur.ssb.shex.domain.ShapeAnd;
import nl.wur.ssb.shex.domain.ShapeNot;
import nl.wur.ssb.shex.domain.ShapeOr;
import nl.wur.ssb.shex.domain.TripleConstraint;
import nl.wur.ssb.shex.domain.shapeExpr;
import nl.wur.ssb.shex.domain.tripleExpression;

/*
 * This is an untested pseudo code solving the recursion problem(https://github.com/shexSpec/shex.js/issues/14) for validating ShEx. Hopefully is does not contain any critical 
 * errors/mistakes, as I still need to test it. 
 * 
 * The principle is based on the creation of a logic graph of condition. This graph represented by nodes of the
 * ConditionalNode class. Each node can have zero or more 
 * childs and can have one of the following op modes (and | xor | or | not | true | false). Each shape to node mapping 
 * will have link to its conditional node in the graph. 
 * Through a element wise validation of all the shape to node mappings a graph is created. 
 * For each new shape to node mapping a new element is added to the shapemap, so the shapemap will grow as the validation runs.
 * 
 * At the end we solve the ConditionalNode logic graph containing nodes with state (and, or, xor, not, true and false),
 * such that we maximize ... 
 * - the number of shape to node mapping with the state conformant? (or whatever definition will be put in the specs)
 * 
 * For simplicity I left out any optimalizations.
 */
public class Validator
{
  private RDFSimpleCon graph;
  private ShapeMap map;
  
  public Validator(ShapeMap map,RDFSimpleCon graph)
  {
    this.graph = graph;
    this.map = map;
  }
  
  public ShapeMap validate()
  {
    //Keep validating until all open items has been validated
    while(true)
    {
      ShapeMapItem item = map.getOpenShapeMapItem();
      if(item == null)
        break;
      this.validate(item);
    }
    //At this point none of the conditionalNode in the graph of conditions should be in the open state
    
    //A piece of code can be included here, which handles the closed shape validation. Removing any conditions that violate the close 
    //shape constraint.
    
    /*
     * Then solve the ConditionalNode logic graph containing nodes with state (and, or, xor, not, true and false),
     * such that we maximize ... 
     * - the number of shape to node mapping with the state conformant?
     */
    //
    
    return map;
  }
  
  private void validate(ShapeMapItem item)
  {
    if(item.shape instanceof Shape)
    {
      Shape shape = (Shape)item.shape;
      tripleExpression expr = shape.getExpression();
      validateTripleExpression(expr,item.state,item.iriNode);
    }
    else if(item.shape instanceof ShapeAnd)
    {
      ShapeAnd shapeAnd = (ShapeAnd)item.shape;
      item.state.opType = "and";
      for(shapeExpr expr : shapeAnd.getAllShapeExprs())
      {
        this.validateShapeExpr(expr,item.state,this.graph.createResource(item.iriNode));
      }
    }
    else if(item.shape instanceof ShapeOr)
    {
      ShapeOr shapeOr = (ShapeOr)item.shape;
      item.state.opType = "or";
      for(shapeExpr expr : shapeOr.getAllShapeExprs())
      {
        this.validateShapeExpr(expr,item.state,this.graph.createResource(item.iriNode));
      }
    }
    else if(item.shape instanceof ShapeNot)
    {
      ShapeNot shapeNot = (ShapeNot)item.shape;
      item.state.opType = "not";
      this.validateShapeExpr(shapeNot.getShapeExpr(),item.state,this.graph.createResource(item.iriNode));
    }
  }
  private void validateTripleExpression(tripleExpression expr,ConditionalNode rootValidation,String nodeIri)
  {
    if(expr == null)
      return;
    if(expr instanceof TripleConstraint)
    {
      TripleConstraint constraint = (TripleConstraint)expr;
       int count = 0;
      //If negated add a negate operation in between
      if(constraint.getNegated())
      {
        ConditionalNode negValidation = new ConditionalNode();
        negValidation.opType = "and";
        rootValidation.opType = "not";
        rootValidation.childs.add(negValidation);
      }
      else
        rootValidation.opType = "and";
      StmtIterator itt = this.graph.listPattern(nodeIri,constraint.getPredicate(),null);
      while(itt.hasNext())
      {
        //we assume that valueExpr is never == null
        this.validateShapeExpr(constraint.getValueExpr(),rootValidation,itt.next().getObject());
        count++;
      }
      //check multiplicity
      if((constraint.getMin() != null && count < constraint.getMin()) 
          || (constraint.getMax() != null && count > constraint.getMax()))
      {
        rootValidation.childs.clear();
        rootValidation.opType = "false";
      }
      
    }
    else if(expr instanceof OneOf)
    {
      OneOf oneOf = (OneOf)expr;
      rootValidation.opType = "xor";
      if(oneOf.getMin() == 1 && oneOf.getMax() == 1)
      {
        for(tripleExpression child : oneOf.getAllExpressions())
        {
          ConditionalNode subValidation = new ConditionalNode();
          rootValidation.childs.add(subValidation);
          this.validateTripleExpression(child,subValidation,nodeIri);
        }
      }
      else
      {
        //Do complex stuff, related to issue https://github.com/shexSpec/shex.js/issues/16
        //All possible solutions should be added to the graph
        /* For example, deal with:
         * ShapeTest {
         *   (ex:prop1 xsd:String, 
         *    ex:prop2 xsd:String+)+
         * }
         */
      }
    }
    else if(expr instanceof EachOf)
    {
      OneOf oneOf = (OneOf)expr;
      rootValidation.opType = "and";
      if(oneOf.getMin() == 1 && oneOf.getMax() == 1)
      {
        for(tripleExpression child : oneOf.getAllExpressions())
        {
          ConditionalNode subValidation = new ConditionalNode();
          rootValidation.childs.add(subValidation);
          this.validateTripleExpression(child,subValidation,nodeIri);
        }
      }
      else
      {
        //Do complex stuff 
      }
    }
    else
      throw new RuntimeException("illegal state");
  }
  
  private void validateShapeExpr(shapeExpr expr,ConditionalNode rootValidation,RDFNode node)
  {    
    if(expr instanceof NodeConstraint)
    {
      //Node constraint resolve easily either to true or false, no recursion here :)
      ConditionalNode subValidation = new ConditionalNode();
      subValidation.opType = this.validateNodeConstraint((NodeConstraint)expr,node) ? "true" : "false";
      rootValidation.childs.add(subValidation);
    }
    else if(expr instanceof Shape || expr instanceof ShapeAnd || expr instanceof ShapeOr || expr instanceof ShapeNot)
    {
      //Get if exsisting otherwise make a shapemap item for given shape to node mapping
      if(!node.isResource())
      {
        ConditionalNode subValidation = new ConditionalNode();
        subValidation.opType = "false";
        rootValidation.childs.add(subValidation);
      }
      else
      {
        //Link the conditional tree to next conditional trees, forming a complex graph 
        ShapeMapItem item = this.map.getMakeMapItem(expr,node.asResource().getURI());
        rootValidation.childs.add(item.state);
      }
    }
    else //External shape ignored in this example
      throw new RuntimeException("illegal state");
  }
  private boolean validateNodeConstraint(NodeConstraint constraint,RDFNode node)
  {
    //PSEUDO: Do the node validation 
    return true;
  }
}
