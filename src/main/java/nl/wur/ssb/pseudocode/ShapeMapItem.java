package nl.wur.ssb.pseudocode;

import nl.wur.ssb.shex.domain.Shape;

public class ShapeMapItem
{
  Shape shape;
  String iriNode;
  ConditionalNode state;
  
  public ShapeMapItem()
  {
    state = new ConditionalNode();
    state.shapeMapItem = this;
  }
  
  public String returnState()
  {
    if(state.opType == "open")
      return "open";
    else if(state.opType == "true")
      return "conformant";
    else if(state.opType == "false")
      return "nonconformant";
    else 
      return "pending";
    
  }
}
