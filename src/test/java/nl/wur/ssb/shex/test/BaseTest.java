package nl.wur.ssb.shex.test;

import java.io.FileWriter;

import org.apache.commons.lang3.StringUtils;

import junit.framework.TestCase;
import nl.wur.ssb.RDFSimpleCon.RDFSimpleCon;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;
import nl.wur.ssb.shex.writer.ShExCWriter;

public class BaseTest extends TestCase
{ 
  public void test1() throws Exception
  {
    RDFSimpleCon con = new RDFSimpleCon("file://GBOLShExR.ttl");
    Domain domain = new Domain(con);
    ShExCWriter writer = new ShExCWriter(new FileWriter("outtest.shex"),domain);
    writer.write();
    writer.close();
    con.close();
  }
}
