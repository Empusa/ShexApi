#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Pull latest
git -C "$DIR" pull

gradle build -b "$DIR/build.gradle" -x test

#mv $DIR/build/libs/ShexApi.jar $DIR/

mvn install:install-file -Dfile=$DIR/build/libs/ShexApi-0.1.jar -DgroupId=nl.wur.ssb.ShexApi -DartifactId=ShexApi -Dversion=0.1 -Dpackaging=jar
